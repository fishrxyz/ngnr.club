import os

import boto3

USER_POOL_ID = os.getenv("NGNR_PROD_USER_POOL")


def list_users(client, user_pool_id: str, filter_expr: str = "") -> list:
    result = client.list_users(UserPoolId=user_pool_id, Filter=filter_expr)
    return result["Users"], result["PaginationToken"]


def print_user_info(users):
    for user in users:
        username = user["Username"]
        for attr in user["Attributes"]:
            if attr["Name"] == "email_verified" and attr["Value"] == "false":
                email = [
                    attr["Value"]
                    for attr in user["Attributes"]
                    if attr["Name"] == "email"
                ]
                try:
                    print([username, email[0]])
                except IndexError:
                    continue


if __name__ == "__main__":
    cognito = boto3.client("cognito-idp")

    print("[INFO] - Listing users ...")

    response = cognito.list_users(UserPoolId=USER_POOL_ID)
    token = response["PaginationToken"]
    users = response["Users"]

    print_user_info(users)
    response = cognito.list_users(UserPoolId=USER_POOL_ID, PaginationToken=token)
    users = response["Users"]
    token = response["PaginationToken"]

    print_user_info(users)

    response = cognito.list_users(UserPoolId=USER_POOL_ID, PaginationToken=token)
    users = response["Users"]
    token = response["PaginationToken"]

    print_user_info(users)
