import boto3

welcome_template = """
<!DOCTYPE html>
<html>
    <head>
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Lekton:wght@400;700&display=swap" rel="stylesheet">

        <style>
            * {
                font-family: 'Lekton', monospace;
            }

            .article {
                margin-bottom: 36px;
            }

            p {
                font-size: 14px;
                line-height: 1.5;
            }

            a {
                font-weight: bold;
            }

            #twitter { color: #1DA1F2; }
            #email { color: #ff80c0; }
            #discord { color: #7289da; }
        </style>
    </head>

    <body>
        <h1>Welcome to the club !</h1>
        <img href="https://media.tenor.com/CBxyvlf0CMoAAAAC/welcome-anime.gif" />
        <h2>Hey @{{username}},</h2>
        <div class="article">
            <p>Thank you for confirming your account. You're officially part of the ngnr.club !</p>
            <p>Now that you're settled, you can take a few minutes to update your profile, add your CV, set your work preferences and showcase your best projects.</p>
            <p>The more info you add, the better your profile will look so don't wait !</p>
        </div>
        <div class="article">
            <p>For questions and feedback, you can reach me at <a id="email" href="mailto:karim@ngnr.club">karim@ngnr.club</a> or on the <a id="discord" href="https://discord.gg/zHnwbVHPUX">official discord server</a>.</p>
            <p>We post frequent updates to <a id="twitter" href="https://twitter.com/ngnrclub">twitter</a> so check us out over there too !</p>
            <p>We're constantly trying to improve the product and experience with new features and we're glad you like it!</p>
       </div>
       <div class="article">
        <p>Hope you have fun with us,</p>
        <p>- Karim of ngnr.club</p>
       </div>
    </body>
</html>
"""

data_export = """
<!DOCTYPE html>
<html>
    <head>
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Lekton:wght@400;700&display=swap" rel="stylesheet">

        <style>
            * {
                font-family: 'Lekton', monospace;
            }

            .article {
                margin-bottom: 20px;
            }

            p {
                font-size: 14px;
                line-height: 1.5;
            }

            a {
                font-weight: bold;
            }

            #email { color: #ff80c0; }
        </style>
    </head>

    <body>
        <h1>Your data export is ready !</h1>
        <h2>Hey @{{username}}</h2>
        <div class="article">
            <p>Your data export is ready for download!</p>
            <p>You will find the file attached to this email</p>
        </div>
        <div class="article">
            <p>If you want to request another export, you can do so from the <a href="https://ngnr.club/settings">settings page</a></p>
        </div>
        <div class="article">
            <p>Have Fun</p>
            <p>- Team ngnr.club</p>
        </div>
    </body>
</html>
"""


def new_template(client, name: str, subject: str, html: str):
    client.create_email_template(
        TemplateName=name, TemplateContent={"Subject": subject, "Html": html}
    )


def update_template(client, name: str, subject: str, html: str):
    client.update_email_template(
        TemplateName=name, TemplateContent={"Subject": subject, "Html": html}
    )


def send(client, template_name: str, template_data: str):
    client.send_email(
        FromEmailAddress="ngnr.club@gmail.com",
        Destination={
            "ToAddresses": [
                "dekooks221@gmail.com",
            ]
        },
        Content={
            "Template": {"TemplateName": template_name, "TemplateData": template_data}
        },
    )


if __name__ == "__main__":
    ses = boto3.client("sesv2")

    # new_template(ses, "data_export", "Your data export is ready !", data_export)
    update_template(ses, "welcome", "Welcome to the club !", welcome_template)

    # send(ses, "welcome", '{"username": "boto3"}')
