import sys
from pprint import pprint

import boto3
from boto3.dynamodb.conditions import Attr

TABLE_NAME = "ngnr-club-prod"


if __name__ == "__main__":
    ddb = boto3.resource("dynamodb")
    table = ddb.Table(TABLE_NAME)

    result = table.scan(
        FilterExpression=Attr("cv").exists()
        & Attr("work_preferences").not_exists()
        & Attr("notifications").not_exists()
    )
    pprint("[INFO] Scanning table for empty cv/work_preferences/notifications ...")
    rows = result["Items"]
    result_set = len(rows)

    pprint(f"[INFO] found {result_set} rows")

    if result_set == 0:
        sys.exit(0)
    else:
        pprint("[INFO] running DB migration ...")
        cv = {"url": "", "visible": False}
        work_preferences = {
            "available": False,
            "freelance": False,
            "eu_ok": False,
            "us_ok": False,
            "remote_ok": False,
            "freelance_min_rate": 0,
            "freelance_fixed_rate": 0,
            "freelance_min_hours": 0,
            "salary_min": 0,
            "salary_max": 0,
        }
        notifications = {
            "community": {"BOOL": False},
            "jobs": {"BOOL": True},
            "product_updates": {"BOOL": True},
        }

        #     # for each row that contains non empty projects
        for row in rows:
            key = row["sub"]

            pprint(f"[INFO] updating row: {key}")
            table.update_item(
                Key={"sub": key},
                UpdateExpression="SET cv = :cv, work_preferences = :wp, notifications = :notifs",
                ExpressionAttributeValues={
                    ":cv": cv,
                    ":wp": work_preferences,
                    ":notifs": notifications,
                },
            )
            pprint(f"[INFO] row: {key} successfully updated !\n")

        pprint("[INFO] Job Done !")
