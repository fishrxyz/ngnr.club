import boto3

BUCKET_NAME = "ngnr-data-staging"


def list_empty_objects(client, bucket: str) -> list:
    objects = client.list_objects_v2(Bucket=bucket)
    empty_objects = filter(lambda obj: obj["Size"] == 0, objects["Contents"])
    result = map(lambda obj: dict(Key=obj["Key"]), empty_objects)
    return list(result)


def delete_empty_objects(client, bucket: str, objects: list) -> list:
    delete_params = {"Objects": objects}
    response = client.delete_objects(Bucket=bucket, Delete=delete_params)
    return response.get("Deleted"), response.get("Errors", [])


if __name__ == "__main__":
    s3 = boto3.client("s3")

    print(f"[INFO] Listing empty files in bucket: {BUCKET_NAME}")
    empty_objects = list_empty_objects(s3, BUCKET_NAME)
    print(f"[INFO] Found {len(empty_objects)} empty files")

    if len(empty_objects) > 0:
        print(f"[INFO] Deleting {len(empty_objects)} empty files ...")
        deleted, errors = delete_empty_objects(s3, BUCKET_NAME, empty_objects)
        print(f"[INFO] Deleted {len(deleted)} files with {len(errors)} errors ...")

    print("[INFO] Operation completed")
