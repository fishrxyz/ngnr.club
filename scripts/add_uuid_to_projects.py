import sys
import uuid
from pprint import pprint

import boto3
from boto3.dynamodb.conditions import Attr

TABLE_NAME = "ngnr-club-prod"

if __name__ == "__main__":
    ddb = boto3.resource("dynamodb")
    table = ddb.Table(TABLE_NAME)

    result = table.scan(FilterExpression=Attr("projects").attribute_type("L"))
    pprint("[INFO] Scanning table for non empty projects lists")
    non_empty_projects = list(
        filter(lambda item: len(item["projects"]) > 0, result["Items"])
    )
    empty_projects_size = len(non_empty_projects)
    pprint(f"[INFO] found {empty_projects_size} rows")

    if empty_projects_size == 0:
        sys.exit(0)
    else:
        pprint("[INFO] adding uuid attribute to existing projects")

        # for each row that contains non empty projects
        for row in non_empty_projects:
            key = row["sub"]
            projects_list = row["projects"]

            # loop through each project and add a UUID
            for proj in projects_list:
                if "uuid" in proj.keys():
                    pprint(f"[INFO] found uuid in project {proj['name']}. Skipping.")
                    continue
                else:
                    proj.update({"uuid": str(uuid.uuid4())})

                    # update each row with the new projects
                    pprint(f"[INFO] updating row: {key}")
                    table.update_item(
                        Key={"sub": key},
                        UpdateExpression="SET projects = :updated_projects",
                        ExpressionAttributeValues={":updated_projects": projects_list},
                    )
                    pprint(f"[INFO] row: {key} successfully updated !\n")
