import os
from pprint import pprint

import boto3

USER_POOL_ID = os.getenv("NGNR_STAGING_USER_POOL")


def get_attribute_names(user_attributes: list) -> list:
    return list(map(lambda attrs: attrs["Name"], user_attributes))


def list_users(client, user_pool_id: str, filter_expr: str = "") -> list:
    result = client.list_users(UserPoolId=user_pool_id, Filter=filter_expr)
    return result["Users"]


def set_preferred_username_attribute(client, user_pool_id: str, username: str):
    try:
        client.admin_update_user_attributes(
            UserPoolId=user_pool_id,
            Username=username,
            UserAttributes=[
                {"Name": "preferred_username", "Value": username},
            ],
        )
    except Exception as e:
        print(f"[ERROR] - {str(e)}")


if __name__ == "__main__":
    cognito = boto3.client("cognito-idp")

    print("[INFO] - Listing users ...")
    users = list_users(
        cognito, USER_POOL_ID, filter_expr="cognito:user_status='CONFIRMED'"
    )

    for user in users:
        username = user["Username"]
        user_attributes = get_attribute_names(user["Attributes"])

        if "preferred_username" in user_attributes:
            pprint("[INFO] - preferred_username attribute is set. Skipping ...")
        else:
            pprint(f"[INFO] - Updating preferred_username for {username} ...")
            set_preferred_username_attribute(cognito, USER_POOL_ID, username)
            pprint(
                f"[INFO] - {username}'s preferred_username attribute has been updated"
            )
