import json
import time

import boto3

sorry_template = """
<!DOCTYPE html>
<html>
    <head>
        <style>
            .article {
                margin-bottom: 36px;
            }

            p {
                font-size: 14px;
                line-height: 1.5;
            }

            a {
                font-weight: bold;
            }

            #twitter { color: #1DA1F2; }
            #email { color: #ff80c0; }
            #discord { color: #7289da; }

        </style>
    </head>

    <body>
        <h1>Welcome to the club !</h1>
        <h2>Hey @{{username}},</h2>
        <div class="article">
            <p>Last week you registered an account with us and we thank you for that ! </p>
            <p>As you are aware, we experienced difficulties with AWS SES and couldn't send the verification e-mails (otherwise known as the HN kiss of death).</p>
            <p>We're happy to let you know that the problem has been fixed and that your account has been made active !</p>
        </div>
        <div class="article">
            <p>You can now login with your username and password and enjoy the site with its newest features.</p>
            <p>We're constantly trying to improve the product and experience with new features and we're glad you like it!</p>
       </div>
       <div class="article">
           <p>For questions and feedback, you can reach me at <a id="email" href="mailto:ngnr.club@gmail.com">this address</a> or on the <a id="discord" href="https://discord.gg/zHnwbVHPUX">official discord server</a>.</p>
           <p>We post frequent updates to <a id="twitter" href="https://twitter.com/ngnrclub">twitter</a> so check us out over there too !</p>
       </div>
       <div class="article">
        <p>We're terribly sorry for the inconvenience and wish you a fantastic week !</p>
        <p>Hope you have fun with us,</p>
        <p>- Karim of ngnr.club</p>
       </div>
    </body>
</html>
"""

users = [
    ["4lch4", "ngnrclub@4lch4.email"],
    ["@justin", "justinsl@gmail.com"],
    ["aaron", "morais.aaron@gmail.com"],
    ["adams", "jadams@protonmail.com"],
    ["air", "morais.aaron@gmail.com"],
    ["ajw", "ajw@toadking.org"],
    ["alex", "pokemon999101+ngnr@gmail.com"],
    ["alexb", "best.alexander@gmail.com"],
    ["alexdenisov", "alex@lowlevelbits.org"],
    ["ali", "ali@aliy.dev"],
    ["amit", "amit@bakhru.in"],
    ["anish", "anish@anishsinghani.com"],
    ["apathor", "mike@faifw.org"],
    ["asddlfhjkgbasdhkl", "test@gmail.com"],
    ["ash", "ashleyjohnson00@gmail.com"],
    ["asinghani", "anish.singhani@gmail.com"],
    ["bard", "bard@disroot.org"],
    ["ben", "ben@processlabs.io"],
    ["bestalex", "best.alexander@gmail.com"],
    ["bird", "properfluffypenguin@proton.me"],
    ["bje", "jaco@nave.moe"],
    ["bobbieg", "dekooks221+fp3@gmail.com"],
    ["borja", "borjam@dagi3d.net"],
    ["bowie", "bowiecd@gmail.com"],
    ["christopher", "kefeli2002@gmail.com"],
    ["chunga", "axxr@tuta.io"],
    ["cls", "crud_revers_03@icloud.com"],
    ["cole", "colereid+ngnr@gmail.com"],
    ["dan.mongoven", "dan.mongoven@gmail.com"],
    ["danielbark", "danba340@gmail.com"],
    ["darryl", "ngnr.club@nixon.mozmail.com"],
    ["degif", "rihards@gravis.lv"],
    ["dylan", "dylans@gmail.com"],
    ["dylans", "dylans@gmail.com"],
    ["eric", "ehamiter@gmail.com"],
    ["erics", "ngnrclub@korni22.org"],
    ["falcon", "properfluffypenguin@proton.me"],
    ["ffflorian", "ngnr@florianimdahl.de"],
    ["frans", "makemeaspam@gmail.com"],
    ["gary", "gary@mcad.am"],
    ["george", "gkunthara1@gmail.com"],
    ["gerard0000", "gmm1990@gmail.com"],
    ["gerard", "gmm1990@gmail.com"],
    ["gpmcadam", "gary@mcad.am"],
    ["gskll", "dev.andrewg@gmail.com"],
    ["hillerliao", "hillerliao@163.com"],
    ["hiyer", "hariharan022@gmail.com"],
    ["hughbien", "hugh@hughbien.com"],
    ["iurii123", "i.kozuliak+spam@gmail.com"],
    ["iurii", "i.kozuliak+spam@gmail.com"],
    ["ivan", "ivan@ivan.build"],
    ["ivanvmoreno", "contacto@ivanmoreno.me"],
    ["j", "joshua@ziggas.com"],
    ["jadams", "jadams@protonmail.com"],
    ["james", "james.long@pm.me"],
    ["jcassee", "joost@cassee.net"],
    ["jcvold", "jcvold@gmail.com"],
    ["jest", "jestics@gmail.com"],
    ["johnbanq", "johnbanq@gmail.com"],
    ["josh", "scjosh2@gmail.com"],
    ["jove", "jove@spucchi.com"],
    ["justin", "justinsl@gmail.com"],
    ["jz", "joshua@ziggas.com"],
    ["kadenbach", "andreas.kadenbach@gmail.com"],
    ["kalib_tweli", "calebfaruki@hey.com"],
    ["kalibtweli", "calebfaruki@hey.com"],
    ["kassiopaea", "k@kgadberry.me"],
    ["kayno", "kayno@kayno.net"],
    ["kinglycrow", "iantbutler01@gmail.com"],
    ["kris", "tiangong00@proton.me"],
    ["lugia", "lugia@proton.me"],
    ["maci", "macis.miri@gmail.com"],
    ["matt", "ngnr@matthew.science"],
    ["mattbee", "matthew@bloch.tv"],
    ["max", "chengmaxwu@gmail.com"],
    ["mew", "lugia@proton.me"],
    ["micha", "yxmicha@gmail.com"],
    ["michab", "yxmicha@gmail.com"],
    ["mickey", "jests.onset-0e@icloud.com"],
    ["nason", "austin.nason@gmail.com"],
    ["netllama", "me@netllama.us"],
    ["nezteb", "ngnr.directed222@simplelogin.com"],
    ["ngnr", "sebastiaan@sevaho.io"],
    ["ngp", "noah@packetlost.dev"],
    ["nico", "nicolasvpadula@gmail.com"],
    ["nikki", "hi@nikki.lol"],
    ["nme", "nme@stepone.no"],
    ["nos", "nosduco@gmail.com"],
    ["owl", "properfluffypenguin@proton.me"],
    ["pablo", "pablosfsanchez@gmail.com"],
    ["parrot", "properfluffypenguin@proton.me"],
    ["pat", "kind.tree0136@fastmail.com"],
    ["patrick", "kind.tree0136@fastmail.com"],
    ["pedro.correa", "pedro8correa@gmail.com"],
    ["penguin", "properfluffypenguin@proton.me"],
    ["piplup", "properfluffypenguin@proton.me"],
    ["pkoch", "ngnr.club@pko.ch"],
    ["plaba", "plaba417@students.bju.edu"],
    ["plantroon", "plantroon@plantroon.com"],
    ["pooralaska", "tahoeschrader@gmail.com"],
    ["puffin", "properfluffypenguin@proton.me"],
    ["rajesh", "rpradhanjan+ngnr.club@gmail.com"],
    ["rishu", "rishu.mehrotra2002@gmail.com"],
    ["rob", "ngnrclub@rob.8shield.net"],
    ["roberto", "ngnrclub@rob.8shield.net"],
    ["robertovillegas", "ngnrclub@rob.8shield.net"],
    ["robin", "properfluffypenguin@proton.me"],
    ["robsy", "roobert@gmail.com"],
    ["rvrx", "colemanning@yandex.com"],
    ["sadmanca", "m.sadman.h@gmail.com"],
    ["sahil", "sahil@ik.me"],
    ["salvatorebarbera", "barbera.salvatore@gmail.com"],
    ["sebastiaan", "sebastiaan@sevaho.io"],
    ["sfk", "suneetkamath@hotmail.com"],
    ["sohail", "sohail.hooda@hotmail.ca"],
    ["someuser", "dekooks221@gmail.com"],
    ["sparrow", "properfluffypenguin@proton.me"],
    ["spucchi", "jove@spucchi.com"],
    ["stephen_lynch", "salynchnew@gmail.com"],
    ["testuser12345", "dekooks221+fp@gmail.com"],
    ["testuser1234", "bobbiegarcia93@gmail.com"],
    ["testuser123", "dekooks221@gmail.com"],
    ["testuser", "dekooks221+fp@gmail.com"],
    ["thefractionalcto", "nico@thefractionalcto.net"],
    ["thehouseplant", "sean.collins@outlook.com"],
    ["thibaut", "thibaut.smith@mailbox.org"],
    ["thomasqbrady", "tqb@thomasqbrady.com"],
    ["tweet", "properfluffypenguin@proton.me"],
    ["u03c6", "fcasco@gmail.com"],
    ["uj", "sudo.screenlife@gmail.com"],
    ["ujj", "sudo.screenlife+ngnr@gmail.com"],
    ["uzantonomon", "uzantonomon@gmail.com"],
    ["verygoodsoftwarenotvirus", "verygoodsoftwarenotvirus@protonmail.com"],
    ["videl", "videl@mailbox.org"],
    ["w", "will@willsmidlein.com"],
    ["ws", "will@willsmidlein.com"],
    ["xe", "chrissycadey@icloud.com"],
    ["yasin", "yasin.celebi@qlub.io"],
    ["yasinn", "yasin.celebi@qlub.io"],
    ["ymc9", "yiming@whimslab.io"],
    ["yurisantos", "yrds96@protonmail.com"],
]


def new_template(client, name: str, subject: str, html: str):
    client.create_email_template(
        TemplateName=name, TemplateContent={"Subject": subject, "Html": html}
    )


def update_template(client, name: str, subject: str, html: str):
    client.update_email_template(
        TemplateName=name, TemplateContent={"Subject": subject, "Html": html}
    )


def send(client, template_name: str, template_data: str, to: str):
    client.send_email(
        FromEmailAddress="ngnr.club@gmail.com",
        Destination={"ToAddresses": [to]},
        Content={
            "Template": {"TemplateName": template_name, "TemplateData": template_data}
        },
    )


if __name__ == "__main__":
    ses = boto3.client("sesv2")

    # new_template(ses, "sorry", "Your account has been activated !", sorry_template)

    # loop through each row and send an email with a second delay between each
    # keep track of then addresses so you don't send duplicates

    for user in users:
        username, email = user
        data = {"username": username}
        send(ses, "sorry", json.dumps(data), email)
        time.sleep(1)
