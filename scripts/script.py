from typing import Dict, List

import boto3

table_name = "ngnr-club-staging"
dynamodb = boto3.resource("dynamodb")
db_table = dynamodb.Table(table_name)

dummy_profiles = [
    {
        "sub": "474a6f8e-ba02-4441-8953-1cf19eb79810",
        "discord": "patatrak#8326",
        "email": "patatrak@gmail.com",
        "credly": "blabla",
        "keybase": "patatrak",
        "username": "patatrak",
        "bio": "I'm a software developer/maker/streamer passionate about the cloud and automation.",
        "github": "https://github.com/patatrak",
        "gitlab": "https://gitlab.com/patatrak",
        "twitch": "https://twitch.tv/patatrak",
        "twitter": "https://twitter.com/patatrak",
        "display_name": "Pata Trak",
        "location": "Cyberspace",
        "position": "Cloud Engineer",
        "personal_site": "https://0xfishr.xyz",
        "blog": "https://0xfishr.xyz/notes",
        "now_page": "https://0xfishr.xyz/now",
        "youtube": "https://youtube.com/@ayofishr",
        "lobsters": "https://youtube.com/@ayofishr",
        "hn": "https://youtube.com/@ayofishr",
        "avatar": "",
        "available": True,
        "freelance": True,
    },
]


def insert_profiles(table, data: List[Dict]):
    """
    Writes dummy profiles to the DynamoDB Table
    """
    print("initialising data ...")
    with table.batch_writer() as batch:
        for item in data:
            batch.put_item(Item=item)
    print("done !")


def init_data(db_client, table, data):
    delete_table(table)
    create_db(db_client=db_client, table_name=table_name)
    insert_profiles(db_table, data)


def create_db(db_client, table_name: str):
    """Creates a new table"""

    print(f"[INFO] Creating Table {table_name}")
    table = db_client.create_table(
        TableName=table_name,
        KeySchema=[{"AttributeName": "sub", "KeyType": "HASH"}],
        AttributeDefinitions=[
            {"AttributeName": "sub", "AttributeType": "S"},
        ],
        ProvisionedThroughput={"ReadCapacityUnits": 1, "WriteCapacityUnits": 1},
    )
    table.wait_until_exists()
    print(f"[INFO] Table {table_name} has been created")


def delete_table(table):
    """Deletes a table and its contents"""
    print("[INFO] Deleting Table ...")
    table.delete()
    table.wait_until_not_exists()
    print("[INFO] Table deleted !")


def main():
    init_data(dynamodb, db_table, dummy_profiles)


if __name__ == "__main__":
    main()
