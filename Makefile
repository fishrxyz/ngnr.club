.PHONY: build clean pkg

RAND_NUM := $(shell shuf -i 1-100000 -n 1 | xargs) 
PKG_NAME := $(shell echo ngnr-$(RAND_NUM).zip | tr -d '[:space:]')

# NOTE: build the code
build: clean
	GOOS=linux GOARCH=amd64 CGO_ENABLED=0 mkdir -p src/build && cd src && go build -o ./build/main ./main.go

# NOTE: package the binary lambda function into a zip file with a random name 
pkg:
	cd ./src/build && zip -r $(PKG_NAME) main

# NOTE: send the packaged build of the function to s3
s3:
	aws s3 cp ./src/build/$(PKG_NAME) s3://ngnr-deploy-cfn/

# NOTE: clean up the mess (compiled binaries and other such files)
clean:
	rm -rf ./src/build/*

send:
	aws cloudformation deploy --template-file infra.cf.yaml --stack-name ngnr-club --region us-east-1 --capabilities CAPABILITY_NAMED_IAM --parameter-overrides NgnrPkgName=$(PKG_NAME)

# NOTE: deploy the thing to AWS using cloudformation
deploy: build pkg s3
	aws cloudformation deploy --template-file infra.cf.yaml --stack-name ngnr-club --region us-east-1 --capabilities CAPABILITY_NAMED_IAM --parameter-overrides NgnrPkgName=$(PKG_NAME)

# NOTE: test the code
test:
	cd src/tests && go test -v ./...
