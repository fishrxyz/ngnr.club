package controllers

import (
	"context"
	"errors"
	"fmt"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/feature/dynamodb/attributevalue"
	"github.com/aws/aws-sdk-go-v2/feature/dynamodb/expression"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb/types"
	"gitlab.com/fishrxyz/ngnr.club/helpers"
	"gitlab.com/fishrxyz/ngnr.club/models"
)

type DynamoDBUpdateItemAPI interface {
	UpdateItem(ctx context.Context, params *dynamodb.UpdateItemInput, optFns ...func(*dynamodb.Options)) (*dynamodb.UpdateItemOutput, error)
}

type ExpressionBuilder func(id string, up *models.UpdateProfile) (expression.Expression, error)

func UpdateProfile(ctx context.Context, api DynamoDBUpdateItemAPI, builder ExpressionBuilder, updateProfile *models.UpdateProfile, authToken, tablename string) (events.APIGatewayProxyResponse, error) {

	var res helpers.Response

	cognitoUserId, err := ExtractCognitoUserIdFromRequest(ctx, authToken)

	if err != nil {
		return res.ServerError(err.Error()), nil
	}

	// build the condition expression
	expr, err := builder(cognitoUserId, updateProfile)

	if err != nil {
		return res.ServerError("[ERROR] - Could not build expression"), nil
	}

	key, err := attributevalue.Marshal(cognitoUserId)
	if err != nil {
		return res.ServerError("[ERROR] - marshal cognitokey"), nil
	}

	input := &dynamodb.UpdateItemInput{
		Key: map[string]types.AttributeValue{
			"sub": key,
		},
		TableName:                 aws.String(tablename),
		UpdateExpression:          expr.Update(),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		ConditionExpression:       expr.Condition(),
		ReturnValues:              types.ReturnValue(*aws.String("ALL_NEW")),
	}

	// run the update against the DB
	update, err := api.UpdateItem(ctx, input)

	if err != nil {
		// check the condition expression error
		var conditionCheckFailed *types.ConditionalCheckFailedException
		if errors.As(err, &conditionCheckFailed) {
			return res.NotFound(fmt.Sprintf("user not found with %s", cognitoUserId)), nil
		}

		msg := fmt.Sprintf("could not update item - %s", err.Error())
		return res.ServerError(msg), nil
	}

	if update.Attributes == nil {
		return res.OK("No fields have been updated"), nil
	}

	updatedProfile := new(models.UpdateProfile)

	// TODO: mock this function too
	err = attributevalue.UnmarshalMap(update.Attributes, &updatedProfile)
	if err != nil {
		return res.ServerError("could not unmarshal attributes"), nil
	}

	res.Data = updatedProfile

	return res.OK("Success HEHE !"), nil
}
