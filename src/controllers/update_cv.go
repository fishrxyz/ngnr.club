package controllers

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"os"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/feature/dynamodb/attributevalue"
	"github.com/aws/aws-sdk-go-v2/feature/dynamodb/expression"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb/types"
	"gitlab.com/fishrxyz/ngnr.club/helpers"
	"gitlab.com/fishrxyz/ngnr.club/models"
)

func UpdateCV(ctx context.Context, api DynamoDBUpdateItemAPI, authToken string, body string) events.APIGatewayProxyResponse {

	// take the auth token
	var res helpers.Response
	var cv models.CV
	tableName := os.Getenv("DB_TABLE_NAME")

	cognitoUserId, err := ExtractCognitoUserIdFromRequest(ctx, authToken)

	if err != nil {
		return res.ServerError(err.Error())
	}

	// take the request body
	jsonErr := json.Unmarshal([]byte(body), &cv)
	if jsonErr != nil {
		return res.ServerError("could not unmarshal json")
	}

	// build the update expression

	key, err := attributevalue.Marshal(cognitoUserId)
	if err != nil {
		return res.ServerError("[ERROR] - could not marshal cognitokey")
	}

	expr, err := expression.NewBuilder().WithUpdate(
		expression.Set(
			expression.Name("cv.url"),
			expression.Value(cv.Url),
		),
	).WithCondition(
		expression.Equal(
			expression.Name("sub"),
			expression.Value(cognitoUserId),
		),
	).Build()

	input := &dynamodb.UpdateItemInput{
		Key: map[string]types.AttributeValue{
			"sub": key,
		},
		TableName:                 aws.String(tableName),
		UpdateExpression:          expr.Update(),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		ConditionExpression:       expr.Condition(),
		ReturnValues:              types.ReturnValue(*aws.String("ALL_NEW")),
	}

	// run the update against the DB
	update, err := api.UpdateItem(ctx, input)

	if err != nil {
		// check the condition expression error
		var conditionCheckFailed *types.ConditionalCheckFailedException
		if errors.As(err, &conditionCheckFailed) {
			return res.NotFound(fmt.Sprintf("user not found with %s", cognitoUserId))
		}

		msg := fmt.Sprintf("could not update item - %s", err.Error())
		return res.ServerError(msg)
	}

	if update.Attributes == nil {
		return res.OK("No fields have been updated")
	}

	updatedProfile := new(models.Profile)

	err = attributevalue.UnmarshalMap(update.Attributes, &updatedProfile)
	if err != nil {
		return res.ServerError("could not unmarshal attributes")
	}

	res.Data = updatedProfile
	// return the updated profile
	// in the front end after call this method, setProfile with the new values

	return res.OK("updated cv")
}
