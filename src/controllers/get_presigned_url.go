package controllers

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"time"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-sdk-go-v2/aws"
	v4 "github.com/aws/aws-sdk-go-v2/aws/signer/v4"
	"github.com/aws/aws-sdk-go-v2/service/s3"
	"github.com/aws/aws-sdk-go-v2/service/s3/types"
	"gitlab.com/fishrxyz/ngnr.club/helpers"
)

type S3PresignPutObjectAPI interface {
	PresignPutObject(ctx context.Context, params *s3.PutObjectInput, optFns ...func(*s3.PresignOptions)) (*v4.PresignedHTTPRequest, error)
}

type PresignedURL struct {
	URL          string      `json:"url"`
	Method       string      `json:"method"`
	SignedHeader http.Header `json:"header"`
}

func GetPresignedURL(ctx context.Context, api S3PresignPutObjectAPI, authToken, mimetype string) events.APIGatewayProxyResponse {

	var res helpers.Response

	cognitoUserId, err := ExtractCognitoUserIdFromRequest(ctx, authToken)

	if err != nil {
		return res.ServerError(err.Error())
	}

	bucketName := os.Getenv("DATA_BUCKET_NAME")
	randomKey := helpers.RandomString(cognitoUserId)

	input := &s3.PutObjectInput{
		Bucket:      aws.String(bucketName),
		Key:         aws.String(randomKey),
		ContentType: aws.String(mimetype),
		ACL:         types.ObjectCannedACL(*aws.String("public-read")),
	}

	preSignedRequest, err := api.PresignPutObject(ctx, input, func(opts *s3.PresignOptions) {
		opts.Expires = time.Duration(60 * int64(time.Second))
	})

	if err != nil {
		msg := fmt.Sprintf("Couldn't get a presigned request to put %v:%v. Here's why: %v\n",
			bucketName, randomKey, err)
		return res.ServerError(msg)
	}

	res.Data = PresignedURL{URL: preSignedRequest.URL, Method: preSignedRequest.Method, SignedHeader: preSignedRequest.SignedHeader}

	return res.OK("url created !")

}
