package controllers

import (
	"context"
	"fmt"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/feature/dynamodb/attributevalue"
	"github.com/aws/aws-sdk-go-v2/feature/dynamodb/expression"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb"
	"gitlab.com/fishrxyz/ngnr.club/helpers"
	"gitlab.com/fishrxyz/ngnr.club/models"
)

type DynamoDBGetItemAPI interface {
	GetItem(ctx context.Context, params *dynamodb.GetItemInput, optFns ...func(*dynamodb.Options)) (*dynamodb.GetItemOutput, error)
}

type DynamoDBScanAPI interface {
	Scan(ctx context.Context, params *dynamodb.ScanInput, optFns ...func(*dynamodb.Options)) (*dynamodb.ScanOutput, error)
}

func GetPublicProfile(ctx context.Context, api DynamoDBScanAPI, tablename, username string) (events.APIGatewayProxyResponse, error) {

	var res helpers.Response
	var profiles []models.Profile

	filterExp := expression.Name("username").Equal(expression.Value(username))
	expr, err := expression.NewBuilder().WithFilter(filterExp).Build()

	result, err := api.Scan(ctx, &dynamodb.ScanInput{
		TableName:                 aws.String(tablename),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		FilterExpression:          expr.Filter(),
	})

	if err != nil {
		return res.ServerError(err.Error()), nil
	}

	// If the result set is empty then we assume it wasn't found
	if result.Count == 0 {
		fmt.Println("NOT FOUND")
		return res.NotFound("This profile does not exist."), nil
	}

	err = attributevalue.UnmarshalListOfMaps(result.Items, &profiles)
	if err != nil {
		return res.ServerError(err.Error()), nil
	}

	res.Data = profiles[0] // We get the first item in the list
	return res.OK("Success !"), nil
}
