package controllers

import (
	"bytes"
	"context"
	"fmt"
	"io/ioutil"
	"os"

	"encoding/json"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/feature/dynamodb/attributevalue"
	"github.com/aws/aws-sdk-go-v2/feature/dynamodb/expression"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb"
	"github.com/aws/aws-sdk-go-v2/service/ses"
	"github.com/aws/aws-sdk-go-v2/service/ses/types"

	"gitlab.com/fishrxyz/ngnr.club/helpers"
	"gitlab.com/fishrxyz/ngnr.club/models"
	"gopkg.in/gomail.v2"
)

type SESSendRawMessageAPI interface {
	SendRawEmail(ctx context.Context, params *ses.SendRawEmailInput, optFns ...func(*ses.Options)) (*ses.SendRawEmailOutput, error)
}

func BuildEmailMessage(sender, recipient, subject, htmlBody, attachement string) types.RawMessage {

	msg := gomail.NewMessage()
	msg.SetHeader("From", sender)
	msg.SetHeader("To", recipient)
	msg.SetHeader("Subject", subject)
	msg.SetBody("text/html", htmlBody)

	msg.Attach(attachement)

	var rawEmail bytes.Buffer
	msg.WriteTo(&rawEmail)

	message := types.RawMessage{
		Data: rawEmail.Bytes(),
	}

	return message
}

func RequestDataExport(ctx context.Context, api DynamoDBScanAPI, emailApi SESSendRawMessageAPI, tablename, authToken string) (events.APIGatewayProxyResponse, error) {

	var response helpers.Response
	var profiles []models.Profile

	// TODO:
	// 1. Extract the user info by using the token
	cognitoUserId, err := ExtractCognitoUserIdFromRequest(ctx, authToken)

	if err != nil {
		return response.ServerError(err.Error()), nil
	}

	// 2. Get the profile associated with the token
	filterExp := expression.Name("sub").Equal(expression.Value(cognitoUserId))
	expr, err := expression.NewBuilder().WithFilter(filterExp).Build()

	result, err := api.Scan(ctx, &dynamodb.ScanInput{
		TableName:                 aws.String(tablename),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		FilterExpression:          expr.Filter(),
	})

	if err != nil {
		return response.ServerError(err.Error()), nil
	}

	// If the result set is empty then we assume it wasn't found
	if result.Count == 0 {
		fmt.Println("NOT FOUND")
		return response.NotFound("This profile does not exist."), nil
	}

	err = attributevalue.UnmarshalListOfMaps(result.Items, &profiles)
	if err != nil {
		return response.ServerError(err.Error()), nil
	}

	userProfile := profiles[0]
	// 3. Marshal that into json
	bytesJson, _ := json.MarshalIndent(profiles[0], "", " ")

	filename := fmt.Sprintf("/tmp/%s-data-export.json", userProfile.Username)

	// 4. Save that into a file into /tmpbytes
	_ = ioutil.WriteFile(filename, bytesJson, 0644)

	sender := os.Getenv("EMAIL_SENDER_ADDRESS")
	recipient := userProfile.Email
	subject := "Your data export is ready !"
	htmlBody := fmt.Sprintf(`
    <html>
      <body>
        <h1>Hey %s!</h1>
        <div>
          <p>Your data export is ready!</p>
          <p>The archive is attached to this e-mail!</p>
        </div>
        <p>High Fives !</p>
        <p>- The ngnr.club team.</p>
      </body>
    </html>
    `, userProfile.Username)

	emailMessage := BuildEmailMessage(sender, recipient, subject, htmlBody, filename)

	msg, err := emailApi.SendRawEmail(ctx, &ses.SendRawEmailInput{
		RawMessage: &emailMessage,
		Destinations: []string{
			*aws.String(recipient),
		},
		Source: aws.String(sender),
	})

	if err != nil {
		errMsg := fmt.Sprintf("Got an error while trying to send email: %s", err.Error())
		return response.ServerError(errMsg), nil
	}

	fmt.Println(msg)

	// 5. Send it as an e-mail with an attachement
	response.Data = "email sent !"

	return response.OK("Success !"), nil
}
