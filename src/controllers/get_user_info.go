package controllers

import (
	"context"
	"errors"
	"fmt"
	"os"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/feature/dynamodb/attributevalue"
	"github.com/aws/aws-sdk-go-v2/feature/dynamodb/expression"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb"
	"github.com/lestrrat-go/jwx/jwk"
	"github.com/lestrrat-go/jwx/jwt"
	"gitlab.com/fishrxyz/ngnr.club/helpers"
	"gitlab.com/fishrxyz/ngnr.club/models"
)

func ExtractCognitoUserIdFromRequest(ctx context.Context, token string) (string, error) {

	defaultRegion := os.Getenv("DEFAULT_REGION")
	userPoolId := os.Getenv("USER_POOL_ID")
	keyUrl := fmt.Sprintf("https://cognito-idp.%s.amazonaws.com/%s/.well-known/jwks.json", defaultRegion, userPoolId)
	keySet, err := jwk.Fetch(ctx, keyUrl)

	if err != nil {
		return "", errors.New("could not fetch public key")
	}

	t := []byte(token)
	parsedToken, err := jwt.Parse(t, jwt.WithKeySet(keySet), jwt.WithValidate(true))

	if err != nil {
		return "", errors.New("Could not parse token")
	}

	sub, _ := parsedToken.Get("sub")

	return sub.(string), nil
}

// TODO: abstract that functionality away to its own function (its used in two different files but we need to move FAST !!)
func GetUserInfo(ctx context.Context, api DynamoDBScanAPI, tablename, authToken string) (events.APIGatewayProxyResponse, error) {

	var res helpers.Response
	var profiles []models.Profile

	cognitoUserId, err := ExtractCognitoUserIdFromRequest(ctx, authToken)

	if err != nil {
		return res.ServerError(err.Error()), nil
	}

	filterExp := expression.Name("sub").Equal(expression.Value(cognitoUserId))
	expr, err := expression.NewBuilder().WithFilter(filterExp).Build()

	result, err := api.Scan(ctx, &dynamodb.ScanInput{
		TableName:                 aws.String(tablename),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		FilterExpression:          expr.Filter(),
	})

	if err != nil {
		return res.ServerError(err.Error()), nil
	}

	// If the result set is empty then we assume it wasn't found
	if result.Count == 0 {
		fmt.Println("NOT FOUND")
		return res.NotFound("This profile does not exist."), nil
	}

	err = attributevalue.UnmarshalListOfMaps(result.Items, &profiles)
	if err != nil {
		return res.ServerError(err.Error()), nil
	}

	res.Data = profiles[0]

	return res.OK("Success !"), nil
}
