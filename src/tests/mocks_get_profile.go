package tests

import (
	"context"

	"github.com/aws/aws-sdk-go-v2/service/dynamodb"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb/types"
	"gitlab.com/fishrxyz/ngnr.club/controllers"
)

type mockDynamoDBGetItemAPI func(ctx context.Context, params *dynamodb.GetItemInput, optFns ...func(*dynamodb.Options)) (*dynamodb.GetItemOutput, error)
type mockDynamoDBScanAPI func(ctx context.Context, params *dynamodb.ScanInput, optFns ...func(*dynamodb.Options)) (*dynamodb.ScanOutput, error)

func (db mockDynamoDBGetItemAPI) GetItem(ctx context.Context, params *dynamodb.GetItemInput, optFns ...func(*dynamodb.Options)) (*dynamodb.GetItemOutput, error) {
	// we return a call to the function which itself returns (*dynamodb.GetItemOutput, error)
	return db(ctx, params, optFns...)
}

func (db mockDynamoDBScanAPI) Scan(ctx context.Context, params *dynamodb.ScanInput, optFns ...func(*dynamodb.Options)) (*dynamodb.ScanOutput, error) {
	return db(ctx, params, optFns...)
}

// Get Profile Mocks
func MockGetProfileOKResponse(username string) controllers.DynamoDBScanAPI {
	return mockDynamoDBScanAPI(func(ctx context.Context, params *dynamodb.ScanInput, optFns ...func(*dynamodb.Options)) (*dynamodb.ScanOutput, error) {
		// t.Helper()
		return &dynamodb.ScanOutput{
			Count: 1,
			Items: []map[string]types.AttributeValue{
				{"username": &types.AttributeValueMemberS{
					Value: username,
				},
				},
			},
		}, nil
	})
}

func MockGetProfileNotFoundResponse(username string) controllers.DynamoDBScanAPI {
	return mockDynamoDBScanAPI(func(ctx context.Context, params *dynamodb.ScanInput, optFns ...func(*dynamodb.Options)) (*dynamodb.ScanOutput, error) {
		return &dynamodb.ScanOutput{
			Items: []map[string]types.AttributeValue{},
		}, nil
	})
}

func MockGetProfileError(username string) controllers.DynamoDBScanAPI {
	return mockDynamoDBScanAPI(func(ctx context.Context, params *dynamodb.ScanInput, optFns ...func(*dynamodb.Options)) (*dynamodb.ScanOutput, error) {
		return &dynamodb.ScanOutput{
			Items: []map[string]types.AttributeValue{},
		}, DynamoDBError
	})
}
