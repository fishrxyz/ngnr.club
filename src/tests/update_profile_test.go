package tests

import (
	"bytes"
	"context"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"testing"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-sdk-go-v2/feature/dynamodb/expression"
	"github.com/stretchr/testify/assert"
	"gitlab.com/fishrxyz/ngnr.club/controllers"
	"gitlab.com/fishrxyz/ngnr.club/helpers"
	"gitlab.com/fishrxyz/ngnr.club/models"
)

func TestUpdatePublicProfileOK(t *testing.T) {

	newProfile := models.UpdateProfile{
		Username: "ayofishr",
		Github:   "Bro",
		Codepen:  "This is it",
	}

	testcase := UpdateTestCase{
		dbClient:    MockUpdateProfileOKResponse,
		tablename:   "test_table",
		username:    "myuser",
		userId:      "1",
		description: "assert HTTP 200 OK",
		profile:     &newProfile,
		expressionBuilder: func(id string, up *models.UpdateProfile) (expression.Expression, error) {
			return expression.Expression{}, nil
		},
		expected: &events.APIGatewayProxyResponse{
			StatusCode: http.StatusOK,
		},
	}

	ctx := context.TODO()
	res, _ := controllers.UpdateProfile(
		ctx,
		testcase.dbClient(testcase.profile),
		testcase.expressionBuilder,
		testcase.profile,
		testcase.userId,
		testcase.tablename)

	assert.Equalf(t, res.StatusCode, testcase.expected.StatusCode, testcase.description)

	// marshal res.body into a models.Profile object and check the usernames
	var resProfile helpers.Response
	body, _ := ioutil.ReadAll(bytes.NewReader([]byte(res.Body)))
	if err := json.Unmarshal(body, &resProfile); err != nil { // Parse []byte to the go struct pointer
		log.Println(err)
		log.Fatal("Cannot unmarshal JSON")
	}

	resUsername := resProfile.Data.(map[string]interface{})["username"].(string)
	resGithub := resProfile.Data.(map[string]interface{})["github"].(string)
	resCodepen := resProfile.Data.(map[string]interface{})["codepen"].(string)

	assert.Equalf(t, resUsername, newProfile.Username, testcase.description)
	assert.Equalf(t, resGithub, newProfile.Github, testcase.description)
	assert.Equalf(t, resCodepen, newProfile.Codepen, testcase.description)

}

func TestUpdatePublicProfileOKNilAttributes(t *testing.T) {

	newProfile := models.UpdateProfile{
		Username: "ayofishr",
		Github:   "Bro",
		Codepen:  "This is it",
	}

	testcase := UpdateTestCase{
		dbClient:    MockUpdateProfileOKNilAttrsResponse,
		tablename:   "test_table",
		username:    "myuser",
		userId:      "1",
		description: "assert HTTP 200 OK with nil attributes",
		profile:     &newProfile,
		expressionBuilder: func(id string, up *models.UpdateProfile) (expression.Expression, error) {
			return expression.Expression{}, nil
		},
		expected: &events.APIGatewayProxyResponse{
			StatusCode: http.StatusOK,
		},
	}

	ctx := context.TODO()
	res, _ := controllers.UpdateProfile(
		ctx,
		testcase.dbClient(testcase.profile),
		testcase.expressionBuilder,
		testcase.profile,
		testcase.userId,
		testcase.tablename)

	assert.Equalf(t, res.StatusCode, testcase.expected.StatusCode, testcase.description)

	// marshal res.body into a models.Profile object and check the usernames
	var resProfile helpers.Response
	body, _ := ioutil.ReadAll(bytes.NewReader([]byte(res.Body)))
	if err := json.Unmarshal(body, &resProfile); err != nil { // Parse []byte to the go struct pointer
		log.Println(err)
		log.Fatal("Cannot unmarshal JSON")
	}

	expectedMsg := "No fields have been updated"
	resMsg := resProfile.Message

	assert.Equalf(t, expectedMsg, resMsg, testcase.description)
}

func TestUpdatePublicProfileNotFound(t *testing.T) {

	newProfile := models.UpdateProfile{
		Username: "ayofishr",
		Github:   "Bro",
		Codepen:  "This is it",
	}

	testcase := UpdateTestCase{
		dbClient:    MockUpdateProfileNotFoundResponse,
		tablename:   "test_table",
		username:    "myuser",
		userId:      "1",
		description: "assert HTTP 404 NotFound",
		profile:     &newProfile,
		expressionBuilder: func(id string, up *models.UpdateProfile) (expression.Expression, error) {
			return expression.Expression{}, nil
		},
		expected: &events.APIGatewayProxyResponse{
			StatusCode: http.StatusNotFound,
		},
	}

	ctx := context.TODO()
	res, _ := controllers.UpdateProfile(
		ctx,
		testcase.dbClient(testcase.profile),
		testcase.expressionBuilder,
		testcase.profile,
		testcase.userId,
		testcase.tablename)

	assert.Equalf(t, res.StatusCode, testcase.expected.StatusCode, testcase.description)

	// marshal res.body into a models.Profile object and check the usernames
	var resProfile helpers.Response
	body, _ := ioutil.ReadAll(bytes.NewReader([]byte(res.Body)))
	if err := json.Unmarshal(body, &resProfile); err != nil { // Parse []byte to the go struct pointer
		log.Println(err)
		log.Fatal("Cannot unmarshal JSON")
	}

	expectedMsg := "user not found"
	resMsg := resProfile.Message

	assert.Equalf(t, expectedMsg, resMsg, testcase.description)
}

func TestUpdatePublicProfileError(t *testing.T) {

	newProfile := models.UpdateProfile{
		Username: "ayofishr",
		Github:   "Bro",
		Codepen:  "This is it",
	}

	testcase := UpdateTestCase{
		dbClient:    MockUpdateProfileErrorResponse,
		tablename:   "test_table",
		username:    "myuser",
		userId:      "1",
		description: "assert HTTP 500",
		profile:     &newProfile,
		expressionBuilder: func(id string, up *models.UpdateProfile) (expression.Expression, error) {
			return expression.Expression{}, nil
		},
		expected: &events.APIGatewayProxyResponse{
			StatusCode: http.StatusInternalServerError,
		},
	}

	ctx := context.TODO()
	res, _ := controllers.UpdateProfile(
		ctx,
		testcase.dbClient(testcase.profile),
		testcase.expressionBuilder,
		testcase.profile,
		testcase.userId,
		testcase.tablename)

	assert.Equalf(t, res.StatusCode, testcase.expected.StatusCode, testcase.description)

	// marshal res.body into a models.Profile object and check the usernames
	var resProfile helpers.Response
	body, _ := ioutil.ReadAll(bytes.NewReader([]byte(res.Body)))
	if err := json.Unmarshal(body, &resProfile); err != nil { // Parse []byte to the go struct pointer
		log.Println(err)
		log.Fatal("Cannot unmarshal JSON")
	}

	expectedMsg := "[ERROR] - could not update item - operation error : blah, something happened"
	resMsg := resProfile.Message

	assert.Equalf(t, expectedMsg, resMsg, testcase.description)
}
