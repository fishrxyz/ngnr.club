package tests

import (
	"errors"

	"github.com/aws/aws-lambda-go/events"
	"gitlab.com/fishrxyz/ngnr.club/controllers"
	"gitlab.com/fishrxyz/ngnr.club/models"
)

type TestCase struct {
	dbClient    func(username string) controllers.DynamoDBScanAPI
	tablename   string
	username    string
	expected    *events.APIGatewayProxyResponse
	description string
	err         error
}

type UpdateTestCase struct {
	dbClient          func(up *models.UpdateProfile) controllers.DynamoDBUpdateItemAPI
	tablename         string
	username          string
	userId            string
	profile           *models.UpdateProfile
	expected          *events.APIGatewayProxyResponse
	description       string
	expressionBuilder controllers.ExpressionBuilder
}

var DynamoDBError error = errors.New("dynamodb error")
