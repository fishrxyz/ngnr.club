package tests

import (
	"context"
	"errors"

	"github.com/aws/aws-sdk-go-v2/service/dynamodb"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb/types"
	"github.com/aws/smithy-go"
	"gitlab.com/fishrxyz/ngnr.club/controllers"
	"gitlab.com/fishrxyz/ngnr.club/models"
)

type mockDynamoDBUpdateItemAPI func(ctx context.Context, params *dynamodb.UpdateItemInput, optFns ...func(*dynamodb.Options)) (*dynamodb.UpdateItemOutput, error)

func (db mockDynamoDBUpdateItemAPI) UpdateItem(ctx context.Context, params *dynamodb.UpdateItemInput, optFns ...func(*dynamodb.Options)) (*dynamodb.UpdateItemOutput, error) {
	return db(ctx, params, optFns...)
}

func MockUpdateProfileOKResponse(up *models.UpdateProfile) controllers.DynamoDBUpdateItemAPI {
	return mockDynamoDBUpdateItemAPI(func(ctx context.Context, params *dynamodb.UpdateItemInput, optFns ...func(*dynamodb.Options)) (*dynamodb.UpdateItemOutput, error) {

		return &dynamodb.UpdateItemOutput{
			Attributes: map[string]types.AttributeValue{
				"username": &types.AttributeValueMemberS{
					Value: up.Username,
				},
				"codepen": &types.AttributeValueMemberS{
					Value: up.Codepen,
				},
				"github": &types.AttributeValueMemberS{
					Value: up.Github,
				},
			},
		}, nil
	})
}

func MockUpdateProfileNotFoundResponse(up *models.UpdateProfile) controllers.DynamoDBUpdateItemAPI {
	return mockDynamoDBUpdateItemAPI(func(ctx context.Context, params *dynamodb.UpdateItemInput, optFns ...func(*dynamodb.Options)) (*dynamodb.UpdateItemOutput, error) {

		return nil, &types.ConditionalCheckFailedException{}
	})
}

func MockUpdateProfileErrorResponse(up *models.UpdateProfile) controllers.DynamoDBUpdateItemAPI {
	return mockDynamoDBUpdateItemAPI(func(ctx context.Context, params *dynamodb.UpdateItemInput, optFns ...func(*dynamodb.Options)) (*dynamodb.UpdateItemOutput, error) {

		return nil, &smithy.OperationError{
			OperationName: "blah",
			Err:           errors.New("something happened"),
		}
	})
}

func MockUpdateProfileOKNilAttrsResponse(up *models.UpdateProfile) controllers.DynamoDBUpdateItemAPI {
	return mockDynamoDBUpdateItemAPI(func(ctx context.Context, params *dynamodb.UpdateItemInput, optFns ...func(*dynamodb.Options)) (*dynamodb.UpdateItemOutput, error) {
		return &dynamodb.UpdateItemOutput{
			Attributes: nil,
		}, nil
	})
}
