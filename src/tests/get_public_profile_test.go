package tests

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"testing"

	"github.com/aws/aws-lambda-go/events"
	"github.com/stretchr/testify/assert"
	"gitlab.com/fishrxyz/ngnr.club/controllers"
	"gitlab.com/fishrxyz/ngnr.club/helpers"
)

func TestGetPublicProfileOK(t *testing.T) {

	testcase := TestCase{
		dbClient:    MockGetProfileOKResponse,
		tablename:   "test_table",
		username:    "myuser",
		description: "assert 200 OK",
		err:         nil,
		expected: &events.APIGatewayProxyResponse{
			StatusCode: 200,
		},
	}

	ctx := context.TODO()
	res, err := controllers.GetPublicProfile(ctx, testcase.dbClient(testcase.username), testcase.tablename, testcase.username)

	assert.Equalf(t, res.StatusCode, testcase.expected.StatusCode, testcase.description)
	assert.Equalf(t, err, nil, "assert status ok is OK")

	// marshal res.body into a models.Profile object and check the usernames
	body, err := ioutil.ReadAll(bytes.NewReader([]byte(res.Body)))
	var resProfile helpers.Response
	if err := json.Unmarshal(body, &resProfile); err != nil { // Parse []byte to the go struct pointer
		log.Println(err)
		log.Fatal("Cannot unmarshal JSON")
	}

	resUsername := resProfile.Data.(map[string]interface{})["username"].(string)
	assert.Equalf(t, resUsername, testcase.username, testcase.description)

}

func TestGetPublicProfileNotFound(t *testing.T) {

	testcase := TestCase{
		dbClient:    MockGetProfileNotFoundResponse,
		tablename:   "test_table",
		username:    "myuser",
		description: "assert 404 Not found",
		err:         errors.New("This profile does not exist."),
		expected: &events.APIGatewayProxyResponse{
			StatusCode: 404,
		},
	}

	ctx := context.TODO()
	res, err := controllers.GetPublicProfile(ctx, testcase.dbClient(testcase.username), testcase.tablename, testcase.username)

	assert.Equalf(t, res.StatusCode, testcase.expected.StatusCode, testcase.description)
	assert.Equalf(t, err, nil, testcase.description)

	// marshal res.body into a models.Profile object and check the usernames
	body, err := ioutil.ReadAll(bytes.NewReader([]byte(res.Body)))
	var resProfile helpers.Response
	if err := json.Unmarshal(body, &resProfile); err != nil { // Parse []byte to the go struct pointer
		log.Println(err)
		log.Fatal("Cannot unmarshal JSON")
	}

	errMsg := resProfile.Message
	assert.Equalf(t, errMsg, testcase.err.Error(), testcase.description)

}

func TestGetPublicProfileError(t *testing.T) {

	testcase := TestCase{
		dbClient:    MockGetProfileError,
		tablename:   "test_table",
		username:    "myuser",
		description: "assert 500 error",
		err:         DynamoDBError,
		expected: &events.APIGatewayProxyResponse{
			StatusCode: 500,
		},
	}

	ctx := context.TODO()
	res, err := controllers.GetPublicProfile(ctx, testcase.dbClient(testcase.username), testcase.tablename, testcase.username)

	assert.Equalf(t, res.StatusCode, testcase.expected.StatusCode, testcase.description)
	assert.Equalf(t, err, nil, testcase.description)

	// marshal res.body into a models.Profile object and check the usernames
	body, err := ioutil.ReadAll(bytes.NewReader([]byte(res.Body)))
	var resProfile helpers.Response
	if err := json.Unmarshal(body, &resProfile); err != nil { // Parse []byte to the go struct pointer
		log.Println(err)
		log.Fatal("Cannot unmarshal JSON")
	}

	errMsg := resProfile.Message
	assert.Equalf(t, errMsg, fmt.Sprintf("[ERROR] - %s", testcase.err.Error()), testcase.description)

}
