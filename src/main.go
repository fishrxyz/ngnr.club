package main

import (
	"context"
	"math/rand"
	"os"
	"time"

	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb"
	"github.com/aws/aws-sdk-go-v2/service/s3"
	"github.com/aws/aws-sdk-go-v2/service/ses"
	"gitlab.com/fishrxyz/ngnr.club/app"
)

var App = app.NgnrClub{
	TableName: os.Getenv("DB_TABLE_NAME"),
}

func init() {
	cfg, err := config.LoadDefaultConfig(context.TODO(), config.WithRegion("us-east-1"))
	if err != nil {
		panic(err)
	}

	App.DB = dynamodb.NewFromConfig(cfg)
	App.S3 = s3.NewFromConfig(cfg)
	App.SES = ses.NewFromConfig(cfg)
	App.S3Signer = app.Presigner{Client: s3.NewPresignClient(App.S3)}

}

func main() {
	rand.Seed(time.Now().UnixNano()) // NOTE: this is to change the seed for every run of the helpers.RandomString(function)
	lambda.Start(App.Handler)
}
