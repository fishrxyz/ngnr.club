package models

type WorkPrefs struct {
	Available           bool `json:"available" dynamodbav:"available"`
	Freelance           bool `json:"freelance" dynamodbav:"freelance"`
	EuOK                bool `json:"eu_ok" dynamodbav:"eu_ok"`
	UsOK                bool `json:"us_ok" dynamodbav:"us_ok"`
	RemoteOK            bool `json:"remote_ok" dynamodbav:"remote_ok"`
	MinHourlyRate       int  `json:"freelance_min_rate" dynamodbav:"freelance_min_rate"`
	FixedProjectRate    int  `json:"freelance_fixed_rate" dynamodbav:"freelance_fixed_rate"`
	MinHoursPerContract int  `json:"freelance_min_hours" dynamodbav:"freelance_min_hours"`
	SalaryMin           int  `json:"salary_min" dynamodbav:"salary_min"`
	SalaryMax           int  `json:"salary_max" dynamodbav:"salary_max"`
}
