package models

type Notifications struct {
	Community      bool `json:"community_spotlight" dynamodbav:"community"`
	Jobs           bool `json:"jobs" dynamodbav:"jobs"`
	ProductUpdates bool `json:"product_updates" dynamodbav:"product_updates"`
}
