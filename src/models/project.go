package models

type Project struct {
	Name        string `json:"name" dynamodbav:"name"`
	UUID        string `json:"uuid" dynamodbav:"uuid"`
	Url         string `json:"url" dynamodbav:"url"`
	Repo        string `json:"repo" dynamodbav:"repo"`
	Year        string `json:"year" dynamodbav:"year"`
	Description string `json:"description" dynamodbav:"description"`
}
