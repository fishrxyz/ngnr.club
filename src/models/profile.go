package models

import (
	"github.com/aws/aws-sdk-go-v2/feature/dynamodb/expression"
)

type Profile struct {
	Email           string        `json:"email" dynamodbav:"email"`
	Username        string        `json:"username" dynamodbav:"username"`
	Github          string        `json:"github" dynamodbav:"github"`
	Codepen         string        `json:"codepen" dynamodbav:"codepen"`
	Gitlab          string        `json:"gitlab" dynamodbav:"gitlab"`
	Twitch          string        `json:"twitch" dynamodbav:"twitch"`
	Twitter         string        `json:"twitter" dynamodbav:"twitter"`
	DisplayName     string        `json:"display_name" dynamodbav:"display_name"`
	Location        string        `json:"location" dynamodbav:"location"`
	PersonalSite    string        `json:"personal_site" dynamodbav:"personal_site"`
	Youtube         string        `json:"youtube" dynamodbav:"youtube"`
	Discord         string        `json:"discord" dynamodbav:"discord"`
	Credly          string        `json:"credly" dynamodbav:"credly"`
	KeyBase         string        `json:"keybase" dynamodbav:"keybase"`
	Bio             string        `json:"bio" dynamodbav:"bio"`
	Sub             string        `json:"sub" dynamodbav:"sub"` // NOTE: this field corresponds to the user's id on AWS cognito
	Avatar          string        `json:"avatar" dynamodbav:"avatar"`
	Blog            string        `json:"blog" dynamodbav:"blog"`
	Lobsters        string        `json:"lobsters" dynamodbav:"lobsters"`
	HackerNews      string        `json:"hn" dynamodbav:"hn"`
	NowPage         string        `json:"now_page" dynamodbav:"now_page"`
	Projects        []Project     `json:"projects" dynamodbav:"projects"`
	Position        string        `json:"position" dynamodbav:"position"`
	Notifications   Notifications `json:"notifications" dynamodbav:"notifications"`
	CV              CV            `json:"cv" dynamodbav:"cv"`
	WorkPreferences WorkPrefs     `json:"work_preferences" dynamodbav:"work_preferences"`
}

type UpdateProfile struct {
	NowPage         string        `json:"now_page" dynamodbav:"now_page"`
	Email           string        `json:"email" dynamodbav:"email"`
	Github          string        `json:"github" dynamodbav:"github"`
	Blog            string        `json:"blog" dynamodbav:"blog"`
	Position        string        `json:"position" dynamodbav:"position"`
	Projects        []Project     `json:"projects" dynamodbav:"projects"`
	Codepen         string        `json:"codepen" dynamodbav:"codepen"`
	Gitlab          string        `json:"gitlab" dynamodbav:"gitlab"`
	Discord         string        `json:"discord" dynamodbav:"discord"`
	Credly          string        `json:"credly" dynamodbav:"credly"`
	KeyBase         string        `json:"keybase" dynamodbav:"keybase"`
	Twitch          string        `json:"twitch" dynamodbav:"twitch"`
	Twitter         string        `json:"twitter" dynamodbav:"twitter"`
	DisplayName     string        `json:"display_name" dynamodbav:"display_name"`
	Location        string        `json:"location" dynamodbav:"location"`
	PersonalSite    string        `json:"personal_site" dynamodbav:"personal_site"`
	Youtube         string        `json:"youtube" dynamodbav:"youtube"`
	Lobsters        string        `json:"lobsters" dynamodbav:"lobsters"`
	HackerNews      string        `json:"hn" dynamodbav:"hn"`
	Bio             string        `json:"bio" dynamodbav:"bio"`
	Sub             string        `json:"sub" dynamodbav:"sub"`
	Username        string        `json:"username" dynamodbav:"username"`
	CV              CV            `json:"cv" dynamodbav:"cv"`
	WorkPreferences WorkPrefs     `json:"work_preferences" dynamodbav:"work_preferences"`
	Notifications   Notifications `json:"notifications" dynamodbav:"notifications"`
}

func BuildUpdateExpression(cognitoUserId string, up *UpdateProfile) (expression.Expression, error) {
	expr, err := expression.NewBuilder().WithUpdate(
		expression.Set(
			expression.Name("gitlab"),
			expression.Value(up.Gitlab),
		).Set(
			expression.Name("github"),
			expression.Value(up.Github),
		).Set(
			expression.Name("codepen"),
			expression.Value(up.Codepen),
		).Set(
			expression.Name("bio"),
			expression.Value(up.Bio),
		).Set(
			expression.Name("youtube"),
			expression.Value(up.Youtube),
		).Set(
			expression.Name("personal_site"),
			expression.Value(up.PersonalSite),
		).Set(
			expression.Name("location"),
			expression.Value(up.Location),
		).Set(
			expression.Name("display_name"),
			expression.Value(up.DisplayName),
		).Set(
			expression.Name("twitter"),
			expression.Value(up.Twitter),
		).Set(
			expression.Name("twitch"),
			expression.Value(up.Twitch),
		).Set(
			expression.Name("credly"),
			expression.Value(up.Credly),
		).Set(
			expression.Name("discord"),
			expression.Value(up.Discord),
		).Set(
			expression.Name("keybase"),
			expression.Value(up.KeyBase),
		).Set(
			expression.Name("blog"),
			expression.Value(up.Blog),
		).Set(
			expression.Name("now_page"),
			expression.Value(up.NowPage),
		).Set(
			expression.Name("position"),
			expression.Value(up.Position),
		).Set(
			expression.Name("email"),
			expression.Value(up.Email),
		).Set(
			expression.Name("lobsters"),
			expression.Value(up.Lobsters),
		).Set(
			expression.Name("hn"),
			expression.Value(up.HackerNews),
		).Set(
			expression.Name("projects"),
			expression.Value(up.Projects),
		).Set(
			expression.Name("username"),
			expression.Value(up.Username),
		).Set(
			expression.Name("work_preferences"),
			expression.Value(up.WorkPreferences),
		).Set(
			expression.Name("cv"),
			expression.Value(up.CV),
		).Set(
			expression.Name("notifications"),
			expression.Value(up.Notifications),
		),
	).WithCondition(
		expression.Equal(
			expression.Name("sub"),
			expression.Value(cognitoUserId),
		),
	).Build()

	return expr, err
}
