package models

type CV struct {
	Url     string `json:"url" dynamodbav:"url"`
	Visible bool   `json:"visible" dynamodbav:"visible"`
}
