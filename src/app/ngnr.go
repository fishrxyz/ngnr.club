package app

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-sdk-go-v2/aws"
	v4 "github.com/aws/aws-sdk-go-v2/aws/signer/v4"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb"
	"github.com/aws/aws-sdk-go-v2/service/s3"
	"github.com/aws/aws-sdk-go-v2/service/ses"
	"gitlab.com/fishrxyz/ngnr.club/controllers"
	"gitlab.com/fishrxyz/ngnr.club/helpers"
	"gitlab.com/fishrxyz/ngnr.club/models"
)

type Presigner struct {
	Client *s3.PresignClient
}

// PutObject makes a presigned request that can be used to put an object in a bucket.
// The presigned request is valid for the specified number of seconds.
func (presigner Presigner) PutObject(
	bucketName string, objectKey string, lifetimeSecs int64) (*v4.PresignedHTTPRequest, error) {
	request, err := presigner.Client.PresignPutObject(context.TODO(), &s3.PutObjectInput{
		Bucket: aws.String(bucketName),
		Key:    aws.String(objectKey),
	}, func(opts *s3.PresignOptions) {
		opts.Expires = time.Duration(lifetimeSecs * int64(time.Second))
	})
	if err != nil {
		log.Printf("Couldn't get a presigned request to put %v:%v. Here's why: %v\n",
			bucketName, objectKey, err)
	}
	return request, err
}

type NgnrClub struct {
	DB        *dynamodb.Client
	TableName string
	S3        *s3.Client
	SES       *ses.Client
	S3Signer  Presigner
}

func (ngnr *NgnrClub) Handler(ctx context.Context, req events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	if req.Resource == "/n/{username}" {
		// NOTE: GET PUBLIC PROFILE
		username := req.PathParameters["username"]
		return controllers.GetPublicProfile(ctx, ngnr.DB, ngnr.TableName, username)
	}

	if req.Resource == "/profile/avatar" {

		token := req.Headers["auth"]

		switch req.HTTPMethod {
		case http.MethodGet:
			imgMimeType := "image/*"
			return controllers.GetPresignedURL(ctx, ngnr.S3Signer.Client, token, imgMimeType), nil
		case http.MethodPut:
			return controllers.UpdateAvatar(ctx, ngnr.DB, token, req.Body), nil
		default:
			return events.APIGatewayProxyResponse{
				StatusCode: http.StatusMethodNotAllowed,
				Body:       fmt.Sprintf("Method %s is not allowed", req.HTTPMethod),
			}, nil
		}

	}

	if req.Resource == "/profile/data" {

		token := req.Headers["auth"]

		if req.HTTPMethod == http.MethodGet {

			return controllers.RequestDataExport(ctx, ngnr.DB, ngnr.SES, ngnr.TableName, token)

		} else {
			return events.APIGatewayProxyResponse{
				StatusCode: http.StatusMethodNotAllowed,
				Body:       fmt.Sprintf("Method %s is not allowed", req.HTTPMethod),
			}, nil
		}

	}

	if req.Resource == "/profile/cv" {

		token := req.Headers["auth"]

		switch req.HTTPMethod {
		case http.MethodGet:
			pdfMimeType := "application/pdf"
			return controllers.GetPresignedURL(ctx, ngnr.S3Signer.Client, token, pdfMimeType), nil
		case http.MethodPut:
			return controllers.UpdateCV(ctx, ngnr.DB, token, req.Body), nil
		default:
			return events.APIGatewayProxyResponse{
				StatusCode: http.StatusMethodNotAllowed,
				Body:       fmt.Sprintf("Method %s is not allowed", req.HTTPMethod),
			}, nil
		}

	}

	if req.Resource == "/profile" {

		token := req.Headers["auth"]

		switch req.HTTPMethod {
		// NOTE: PUT /api/profile UpdateProfile
		case http.MethodPut:
			var res helpers.Response
			var updateProfileData models.UpdateProfile
			err := json.Unmarshal([]byte(req.Body), &updateProfileData)
			if err != nil {
				return res.ServerError("could not unmarshal json"), nil
			}
			return controllers.UpdateProfile(ctx, ngnr.DB, models.BuildUpdateExpression, &updateProfileData, token, ngnr.TableName)

		// NOTE: GET /api/profile = GetProfileInfo
		case http.MethodGet:
			return controllers.GetUserInfo(ctx, ngnr.DB, ngnr.TableName, token)

		default:
			return events.APIGatewayProxyResponse{
				StatusCode: http.StatusMethodNotAllowed,
				Body:       fmt.Sprintf("Method %s is not allowed", req.HTTPMethod),
			}, nil
		}

	}

	return events.APIGatewayProxyResponse{
		StatusCode: http.StatusOK,
		Body:       fmt.Sprintf("resource: %s \npath: %s\npath params: %s \nHTTP Method: %s", req.Resource, req.Path, req.PathParameters, req.HTTPMethod),
	}, nil

}
