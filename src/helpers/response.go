package helpers

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/aws/aws-lambda-go/events"
)

type Response struct {
	StatusCode int         `json:"status"`
	Message    string      `json:"message"`
	Data       interface{} `json:"data"`
	Error      bool        `json:"error"`
}

func (res Response) OK(msg string) events.APIGatewayProxyResponse {

	if msg == "" {
		msg = "Success !"
	}

	res.StatusCode = http.StatusOK
	res.Message = msg

	return res.Send()
}

func (res Response) Created(msg string) events.APIGatewayProxyResponse {

	if msg == "" {
		msg = "Created !"
	}

	res.StatusCode = http.StatusCreated
	res.Message = msg

	return res.Send()
}

func (res Response) ServerError(msg string) events.APIGatewayProxyResponse {

	res.StatusCode = http.StatusInternalServerError
	res.Message = fmt.Sprintf("[ERROR] - %s", msg)
	res.Error = true
	res.Data = nil

	return res.Send()
}

func (res Response) NotFound(msg string) events.APIGatewayProxyResponse {

	res.StatusCode = http.StatusNotFound
	res.Message = msg
	res.Error = true
	res.Data = nil

	return res.Send()
}

func (res Response) NotImplemented() events.APIGatewayProxyResponse {

	res.StatusCode = http.StatusNotImplemented
	res.Message = "[error] - Method not implemented"
	res.Error = true
	res.Data = nil

	return res.Send()
}

func (res Response) MethodNotAllowed() events.APIGatewayProxyResponse {

	res.StatusCode = http.StatusMethodNotAllowed
	res.Message = "[error] - Method not allowed"
	res.Error = true
	res.Data = nil

	return res.Send()
}

func (r Response) Send() events.APIGatewayProxyResponse {

	body, err := r.ToJson(r)

	if err != nil {
		log.Fatalf("[Error] Could not marshal response: %s", err.Error())
	}

	return events.APIGatewayProxyResponse{
		Body: string(body),
		Headers: map[string]string{
			"content-type":                 "application/json",
			"Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,auth,Authorization,X-Api-Key,X-Amz-Security-Token,X-Amz-User-Agent",
			"Access-Control-Allow-Origin":  "*",
			"Access-Control-Allow-Methods": "OPTIONS,GET,PUT,POST,DELETE,PATCH,HEAD",
		},
		StatusCode: r.StatusCode,
	}
}

func (r Response) ToJson(body interface{}) ([]byte, error) {

	jsonRes, err := json.Marshal(body)
	if err != nil {
		return []byte{}, err
	}
	return jsonRes, nil
}
