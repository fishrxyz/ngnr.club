# ngnr.club v3

## Useful links

https://github.com/seraphjiang/aws-cors-cloudformation/blob/master/cloudformation.yaml
https://awstut.com/en/2022/05/22/create-rest-api-type-api-gateway-using-cloudformation-en/

## Notes

When deploying a new method, make sure to update the integration request in the staging stage and change the function name to `ngnr${stageVariables.env}`

AND RUN THE STUPID COMMAND ON TOP OF REDEPLOYING THE STAGE

### Cross stack referencing

```bash
aws cloudformation describe-stacks --stack-name ngnr-club --query Stacks[0].Outputs[?OutputKey==`NgnrLogRoleArn`].OutputValue --output text
```
